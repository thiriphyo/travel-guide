json.extract! flight, :id, :name, :location, :contact, :email, :created_at, :updated_at
json.url flight_url(flight, format: :json)
