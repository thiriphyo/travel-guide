json.extract! page, :id, :name, :image, :created_at, :updated_at
json.url page_url(page, format: :json)
