require "application_system_test_case"

class GuesthousesTest < ApplicationSystemTestCase
  setup do
    @guesthouse = guesthouses(:one)
  end

  test "visiting the index" do
    visit guesthouses_url
    assert_selector "h1", text: "Guesthouses"
  end

  test "creating a Guesthouse" do
    visit guesthouses_url
    click_on "New Guesthouse"

    fill_in "Contact", with: @guesthouse.contact
    fill_in "Location", with: @guesthouse.location
    fill_in "Name", with: @guesthouse.name
    click_on "Create Guesthouse"

    assert_text "Guesthouse was successfully created"
    click_on "Back"
  end

  test "updating a Guesthouse" do
    visit guesthouses_url
    click_on "Edit", match: :first

    fill_in "Contact", with: @guesthouse.contact
    fill_in "Location", with: @guesthouse.location
    fill_in "Name", with: @guesthouse.name
    click_on "Update Guesthouse"

    assert_text "Guesthouse was successfully updated"
    click_on "Back"
  end

  test "destroying a Guesthouse" do
    visit guesthouses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Guesthouse was successfully destroyed"
  end
end
