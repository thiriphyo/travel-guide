require 'test_helper'

class GuesthousesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @guesthouse = guesthouses(:one)
  end

  test "should get index" do
    get guesthouses_url
    assert_response :success
  end

  test "should get new" do
    get new_guesthouse_url
    assert_response :success
  end

  test "should create guesthouse" do
    assert_difference('Guesthouse.count') do
      post guesthouses_url, params: { guesthouse: { contact: @guesthouse.contact, location: @guesthouse.location, name: @guesthouse.name } }
    end

    assert_redirected_to guesthouse_url(Guesthouse.last)
  end

  test "should show guesthouse" do
    get guesthouse_url(@guesthouse)
    assert_response :success
  end

  test "should get edit" do
    get edit_guesthouse_url(@guesthouse)
    assert_response :success
  end

  test "should update guesthouse" do
    patch guesthouse_url(@guesthouse), params: { guesthouse: { contact: @guesthouse.contact, location: @guesthouse.location, name: @guesthouse.name } }
    assert_redirected_to guesthouse_url(@guesthouse)
  end

  test "should destroy guesthouse" do
    assert_difference('Guesthouse.count', -1) do
      delete guesthouse_url(@guesthouse)
    end

    assert_redirected_to guesthouses_url
  end
end
