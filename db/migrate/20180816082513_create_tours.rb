class CreateTours < ActiveRecord::Migration[5.2]
  def change
    create_table :tours do |t|
      t.string :name
      t.string :location
      t.string :contact
      t.string :email

      t.timestamps
    end
  end
end
