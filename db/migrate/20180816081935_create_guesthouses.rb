class CreateGuesthouses < ActiveRecord::Migration[5.2]
  def change
    create_table :guesthouses do |t|
      t.string :name
      t.string :location
      t.string :contact

      t.timestamps
    end
  end
end
