class Place < ApplicationRecord
    validates :name, presence: true, length: { minimum: 5 }
  has_one_attached :image
end
