json.extract! tour, :id, :name, :location, :contact, :email, :created_at, :updated_at
json.url tour_url(tour, format: :json)
