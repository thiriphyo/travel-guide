json.extract! home, :id, :title, :image, :created_at, :updated_at
json.url home_url(home, format: :json)
